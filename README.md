# D. A. R. L. I. N. G.
<sup><sup>(Digital Assistant Recognizing Lewd Intent Near Girls)</sup></sup>

Voice assistant for Koikatsu that lets you control sex scenes with voice commands.

Made in an attempt to replace/facilitate Koikatsu's awkward VR control system.

## Dependencies
- [BepInEx](https://github.com/BepInEx/BepInEx/releases) 5.4.12 or later
- [KKAPI](https://github.com/IllusionMods/IllusionModdingAPI/releases) 1.23.0 or later
- [UnityInput](https://github.com/nhydock/BepInEx.UnityInput/releases) 1.0.0 or later (shipped with the release for convenience)

## Supported voice commands
- **`undress`**: undresses the heroine
- **`blowj#b`**, **`missionary`**, **`cowgirl`**, **`doggy`**: tells her which pose to assume (sitting/wall variants also work)
- **`insert`** or **`put it in`**: initiates the standard insertion procedure
- **`faster`**: starts the action, and increases speed if it's already started
- **`slower`**: decreases speed
- **`stronger`**: makes her go harder
- **`weaker`**: makes her go less hard
- **`I'm coming`**: immediately fills the excitement gauges (DARLING locks the gauges automatically if you used the voice command for insertion)

## Supported voice commands (Japanese)
- **`脱いで`**: undresses the heroine
- **`正常位`**, **`騎乗位`**, **`後背位`**, **`フェラ`**: tells her which pose to assume (sitting/wall variants also work)
- **`入れるぞ`**: initiates the standard insertion procedure
- **`もっと早く`**: starts the action, and increases speed if it's already started
- **`もっとゆっくり`**: decreases speed
- **`もっと強く`**: makes her go harder
- **`もっと優しく`**: makes her go less hard
- **`もうダメ`**: immediately fills the excitement gauges
